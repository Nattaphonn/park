const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const sqlServer = require("mssql");

// ----------------- General API -----------------
app.use(bodyParser.json({
  limit: '50mb'
}));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,X-Access-Token,XKey,Authorization");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.listen(1542, function () {
  console.log("Running In Port 1542 In HTTP");
});

// ---------------------- SQL SERVER ----------------------
const config = {
  user: 'new61515003',
  password: 'Dk3gymnatf',
  server: 'smartparking2.database.windows.net',
  database: 'smartparking',
  port: 1434,
  options: {
    encrypt: true,
  },
};


app.get("/api/hello", (req, res) => {

  res.send(JSON.stringify({
    "status": true,
    "msg": "hello",
  }))

});


app.post("/users/sign-up", (req, res) => {
  let Username = req.body.Username;
  let Email = req.body.Email;
  let Password = req.body.Password;
  let PhoneNo = req.body.PhoneNo;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "INSERT INTO Users (Username, Email, Password, PhoneNo) VALUES ('" + Username + "', '" + Email + "', '" + Password + "', '" + PhoneNo + "')";
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);

          if ((err.originalError.info.message).includes('Email_PhoneNo') === true) {
            console.log('Email_PhoneNo'),
              res.send(JSON.stringify({
                "status": false,
                "response": "Email_PhoneNo",
              }));
          } else if ((err.originalError.info.message).includes('Username_UNIQUE') === true) {
            console.log('Username_UNIQUE'),
              res.send(JSON.stringify({
                "status": false,
                "response": "Username_UNIQUE",
              }));
          } else {
            res.send(JSON.stringify({
              "status": false,
              "response": err
            }));
          }
        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": "SIGN UP SUCCESSFUL"
          }));
          sqlServer.close()
        }
      });
    }
  });
});

app.get("/users/user", (req, res) => {
  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT * FROM Users";
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false
          }));
        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": results
          }));
          sqlServer.close()
        }
      });
    }
  });
});


app.post("/users/login", (req, res) => {
  let Username = req.body.Username;
  let Password = req.body.Password;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT *  FROM Users WHERE Username = '" + Username + "' AND Password = '" + Password + "'";
      requestServer.query(sql, (err, results) => {
        console.log(sql);
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));

        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": results.recordset,
            "length": results.recordset.length
          }));
          sqlServer.close()
        }
      });
    }
  });
});


// app.post("/lots/add-lot", (req, res) => {


//   sqlServer.connect(config, (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       let requestServer = new sqlServer.Request();


//       for (let index = 0; index < 8; index++) {
//         let sql = "UPDATE Lots SET Status = 0, UpdateDatetime = CURRENT_TIMESTAMP WHERE LotID = " + (index + 1) ;
//         requestServer.query(sql, (err, results) => {
//           if (err) {
//             console.log(err);
//             res.send(JSON.stringify({
//               "status": false,
//               "response": err
//             }));
//           } else {
//             if (index === 7) {
//               res.send(JSON.stringify({
//                 "status": true,
//                 "response": "UPDATE 8 LOTS SUCCESSFUL"
//               }));
//               sqlServer.close()
//             }
//           }
//         });
//       }
//     }
//   });
// });

app.post("/lots/chk-free-lot", (req, res) => {
  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT * FROM Lots WHERE Status = 1";

      //   for (let index = 0; index < 8; index++) {
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          // if (index === 7) {
          res.send(JSON.stringify({
            "status": true,
            "response": results.recordset
          }));
          sqlServer.close()
          // }
        }
      });
      //     }
    }
  });
});


app.post("/bookings/booking", (req, res) => {
  let LotId = req.body.LotId;
  let UserId = req.body.UserId;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "INSERT INTO Bookings (LotId, UserId, BookingDatetime, BookingStatus, UpdateDatetime, PaymentStatus) VALUES ('" + LotId + "', '" + UserId + "', CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 0)";
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          let sql2 = "UPDATE Lots SET Status = 0 , UpdateDatetime = CURRENT_TIMESTAMP WHERE LotId = " + LotId;
          requestServer.query(sql2, (err, results) => {
            if (err) {
              console.log(err);
              res.send(JSON.stringify({
                "status": false,
                "response": err
              }));
            } else {

              let sql3 = "SELECT TOP 1 * FROM Bookings WHERE LotId = " + LotId + " AND UserId = " + UserId + " AND PaymentStatus = 0 ORDER BY BookingId DESC";

              requestServer.query(sql3, (err, results3) => {
                console.log(sql3)
                if (err) {
                  console.log(err);
                  res.send(JSON.stringify({
                    "status": false,
                    "response": err
                  }));
                } else {
    
                  //let sql3 = "SELECT * FROM Bookings WHERE LotId = " + LotId + " AND UserId = " + UserId + " AND PaymentStatus = 0 ORDER BY LotId DESC"
    
                  res.send(JSON.stringify({
                    "status": true,
                    "response": results3.recordset//"BOOKING AND UPDATE LOT SUCCESSFUL"
                  }));
                  sqlServer.close()
    
                }
              });
            }
          });
        }
      });
    }
  });
});

app.post("/transection/unpaid", (req, res) => {
  let UserId = req.body.UserId;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT * FROM Bookings WHERE PaymentStatus = 0 AND UserId = " + UserId;
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": results.recordset
          }));
        }
      });
    }
  });
});


app.post("/bookings/chk-in", (req, res) => {
  let BookingId = req.body.BookingId;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "UPDATE Bookings SET UpdateDatetime = CURRENT_TIMESTAMP, TimeIn = CURRENT_TIMESTAMP WHERE BookingId = " + BookingId;
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": "UPDATE TIME IN SUCCESSFUL"
          }));
        }
      });
    }
  });
});

app.post("/transection/chk-booking", (req, res) => {
  let BookingId = req.body.BookingId
  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT *, DATEDIFF(MINUTE, TimeIn, TimeOut) AS TimeDiff, (DATEDIFF(MINUTE, TimeIn, TimeOut)) * 0.67 AS Price FROM Bookings WHERE BookingId =" + BookingId;

      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {

          res.send(JSON.stringify({
            "status": true,
            "response": results.recordset
          }));
          sqlServer.close()
          // }
        }
      });
      //     }
    }
  });
});

app.post("/bookings/chk-out", (req, res) => {
  let BookingId = req.body.BookingId;
  let LotId = req.body.LotId;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "UPDATE Bookings SET UpdateDatetime = CURRENT_TIMESTAMP, TimeOut = CURRENT_TIMESTAMP WHERE BookingId = " + BookingId;
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          let sql2 = "UPDATE Lots SET Status = 1 , UpdateDatetime = CURRENT_TIMESTAMP WHERE LotId = " + LotId;
          requestServer.query(sql2, (err, results) => {
            if (err) {
              console.log(err);
              res.send(JSON.stringify({
                "status": false,
                "response": err
              }));
            } else {

              //let sql3 = "SELECT TOP 1 * FROM Bookings WHERE LotId = " + LotId + " AND UserId = " + UserId + " AND PaymentStatus = 0 ORDER BY LotId DESC";

              // requestServer.query(sql3, (err, results3) => {
              //   console.log(sql3)
              //   if (err) {
              //     console.log(err);
              //     res.send(JSON.stringify({
              //       "status": false,
              //       "response": err
              //     }));
              //   } else {
    
                  //let sql3 = "SELECT * FROM Bookings WHERE LotId = " + LotId + " AND UserId = " + UserId + " AND PaymentStatus = 0 ORDER BY LotId DESC"
    
                  res.send(JSON.stringify({
                    "status": true,
                    "response": "UPDATE TIME OUT SUCCESSFUL"
                  }));
                  sqlServer.close()
    
              //   }
              // });
            }
          });



          // res.send(JSON.stringify({
          //   "status": true,
          //   "response": "UPDATE TIME OUT SUCCESSFUL"
          // }));
        }
      });
    }
  });
});

// app.post("/bookings/time-dif", (req, res) => {

//   sqlServer.connect(config, (err) => {
//     if (err) {
//       console.log(err);
//     } else {
//       let requestServer = new sqlServer.Request();
//       let sql = "DATEDIFF ( minute ,  , enddate )";
//       requestServer.query(sql, (err, results) => {
//         if (err) {
//           console.log(err);
//           res.send(JSON.stringify({
//             "status": false,
//             "response": err
//           }));
//         } else {
//           res.send(JSON.stringify({
//             "status": true,
//             "response": "UPDATE TIME OUT SUCCESSFUL"
//           }));
//         }
//       });
//     }
//   });
// });


app.post("/transection/payment", (req, res) => {
  let BookingId = req.body.BookingId;
  let Price = req.body.Price

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "INSERT INTO Transactions (BookingId, Price, CreateDatetime, Status) VALUES (" + BookingId + ", " + Price + ", CURRENT_TIMESTAMP, 1) ";
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          let sql2 = "UPDATE Bookings SET PaymentStatus = 1, UpdateDatetime = CURRENT_TIMESTAMP WHERE BookingId = " + BookingId;
          requestServer.query(sql2, (err, results) => {
            if (err) {
              console.log(err);
              res.send(JSON.stringify({
                "status": false,
                "response": err
              }));
            } else {

              res.send(JSON.stringify({
                "status": true,
                "response": "Transaction AND UPDATE Bookings SUCCESSFUL"
              }));
              sqlServer.close()

            }
          });
        }
      });
    }
  });
});

app.get("/bookings/chk-bookings", (req, res) => {
  //let BookingId = req.body.BookingId;

  sqlServer.connect(config, (err) => {
    if (err) {
      console.log(err);
    } else {
      let requestServer = new sqlServer.Request();
      let sql = "SELECT * FROM Bookings";
      requestServer.query(sql, (err, results) => {
        if (err) {
          console.log(err);
          res.send(JSON.stringify({
            "status": false,
            "response": err
          }));
        } else {
          res.send(JSON.stringify({
            "status": true,
            "response": results.recordset//"UPDATE TIME OUT SUCCESSFUL"
          }));
        }
      });
    }
  });
});
